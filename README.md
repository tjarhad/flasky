## 
This is fork of [flasky repo](https://github.com/miguelgrinberg/flasky)

## Installation 

* Install python 3.7 & python pip 
* Create virtual env: `python3 -m venv venv`
* Activate virtual environment: `source venv/bin/activate`
* Install dependencies: ` pip install -r requirements/dev.txt`


## Running application: 

* `export FLASK_CONFIG=development`
* `export FLASK_APP=flasky.py`

* Create Tables `flask db upgrade`
* Run the application `flask run`
* Deploy `flask deploy`
* Application is running on [localhost:5000](http://localhost:5000/)


## Running Test cases

* `flask test`

